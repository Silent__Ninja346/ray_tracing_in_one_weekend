# Ray Tracing in One (ish) Hour

By David Krauthamer

---

## Get a C Compiler

- Link in the discord or sent as an announcement
- <https://maglit.me/ieee-rt>

---

## So What's a Ray Tracing?

- Rendering done in the same way we see / things are lit IRL
- Follow beams of light on the path they take to get back to our eyes / the
  camera

Note: While we wait for that stuff to install hopefully quickly, here's a brief
history on RT. Yeet out lines from the camera, see what they bounce off of, and
adjust the color the camera "sees" accordingly

---

![rt_diagram](./rt_diagram.png)

<!-- .slide: data-background="#777777" -->

Note: We're going to more or less be doing this, but without the light sources
for the time being, because math and time constraints.

---

## Who Uses It?

- Every animation studio ever
- They've got essentially infinite time to render things, so they can make it
  look pretty
- More recently, video games

Note: This technique is super computationally expensive. Animations use it
because they can spend as much time as they want on each frame. Until recently,
games couldn't afford to do it because you want to get out a consistent number
of frames. Newer hardware is allowing it to be done in real time.

---

## Let's Get Started

- If you followed the install steps you should have it already
- Download the code (link in Discord)
- Next to the blue `Code` button, press the download button
- Choose your preferred archive format
- Unzip that baby somewhere and you're ready to start

Note: This workshop is going to be run really fast, and you're not going to have
time to write everything out yourself. I'm going to be giving a general overview
of the math that goes into RT, then pointing to where in the code the new
concept is implemented.

---

## Building

- Open up a terminal and navigate to the directory with the code.
- On windows this would be an MSYS2 or WSL terminal. MacOS its just called
  terminal. Linux you know what's up.

```sh
meson setup build
cd build
ninja
```

---

## Output an Image

- Open up `src/chapter_02/main.c` in your editor of choice.
- Key things to look at are line 9 and onwards

Note: Iterates over each pixel in the image from top to bottom, and left to
right. Calculates r, g, b values based on the width for r, and height for g.
Then they're converted to numbers between 0 and 255, and printed to the screen.

---

## Running the code

```sh
./chapter_02 > out.ppm
```

Note: If on Windows then chapter_02 will probably be chapter_02.exe. The ">"
symbol is used for redirecting the output of the terminal. Here we are
redirecting stdin (the default) to a file called out.ppm. You'll still see other
stuff printing, that's because it was printed to stderr.

---

## Image Format (.ppm)

- The image format we're using isn't a normal png or jpeg.
- Try this website to view it:
  [https://www.cs.rhodes.edu/welshc/COMP141_F16/ppmReader.html](https://www.cs.rhodes.edu/welshc/COMP141_F16/ppmReader.html)

Note: You can actually open the .ppm file in a text editor, its just plain text!

---

<img class="r-stretch" src="chapter_02.png">

---

## Vector Struct

- Used for colors, coordinates, and directions
- Open up `include/Vec3.h` to see the declarations, and `src/Vec3.c` to see the
  definitions

Note: In C, we group variable types together with a struct. Unlike classes from
languages like Java, Python, etc, there are no methods or inheritance, structs
only hold data. The one cool thing to note is in the .h file, Vec3 is defined as
a union.

---

Worth looking at is the declaration and definition of the Vec3 type itself in
Vec3.h, which is a union.

```c
typedef struct Vec3 {
  union {
    f64 xyz[3];
    struct {
      f64 x;
      f64 y;
      f64 z;
    };
  };
} Vec3;
Vec3 vec = {1, 2, 3};
f64 x = vec.x;
f64 y = vec.xyz[1];
```

Note: A struct contains each of the members listed inside it, and takes up as
much space as all of them together. A union is only ever as large as it's
largest member, and is able to hold one of its members at a time. The two types
listed are an array of 3 floats, and a struct containing 3 floats, which allows
us to access the same data in two different ways.

---

## Ray Struct

![ray struct](./ray.jpg)

$$ P(t)=A+t \* b $$

Note: P is a 3D position along a line in 3D. A is the ray origin and b is the
ray direction.

The parameter t is a real number (f64 or double in code). Plug in different t
values and you get a different point along the ray. Negative t vals allow you to
go anywhere on the 3d line.

---

- Open up `Ray.h` and `Ray.c`
- Just a struct of two `Vec3`s, a function to create one, and a function to do
  the previously shown calculation

---

## Yeeting Rays Into the Scene

![yeeting rays](./yeeting_rays.jpg)

Note: Time to start tracing rays. As said before, we send a ray through each
pixel and computres the color seen in the direction of those rays.

- (1) Calculate the ray from the eye / camera to the pixel
- (2) Determine if it intersects with anything
- (3) Compute a color for that intersection point

We're going to put the camera center at 0, 0, 0 in a 3D space. The y axis goes
up, x axis to the right, into the screen is the negative z. We'll start in the
upper left hand corner and use two offset vectors along the screen sides to move
the ray endpoint across the screen.

Open up chapter 4's main.c, and you'll see the addition of a ray_color function,
new constants at the top of main, and the use of the new function inside the two
for loops.

---

<img class="r-stretch" src="chapter_04.png">

Note: The ray_color function linearly blends white and blue depending on the
height of the y coordinate after scaling the ray direction to unit length
(between -1.0 and 1.0). Because we look at the y height after normalizing the
vector, there's a horizontal gradient to the color as well as the vertical.

We then scaled this value between -1 and 1 to be between 0 and 1, where t=1.0 is
pure blue and t=0.0 is pure white.

---

## Sphere Time

And more math oh boy!

Note: I'm going to speedrun this proof. If it completely goes over your head
that's fine. You can always come back and look at these slides again, and if you
choose to work through this yourself, get out a pencil and paper and do it by
hand.

---

$$ x^2 + y^2 + z^2 = R^2 $$
$$ (x - C_x)^2 + (y - C_y)^2 + (z - C_z)^2 = r^2 $$

Note: First eq is the equation for a sphere of radius R with a center at the
origin

If we move the sphere to coordinates cx, cy, cz, then the equation looks like
this.

In graphics programming we almost always want formulas in terms of vectors so
that the x, y, z stuff is under the hood and dealt with by the Vec3 class.

---

$$
(P - C) \cdot (P - C) =
$$

$$
(x - C_x)^2 + (y - C_y)^2 + (z - C_z)^2
$$

Note: We know that the vector from center C to point P is (P - C), giving the
following

---

$$ (P - C) \cdot (P - C) = r^2 $$

$$
(A + t \* b - C) \cdot (A + t \* b - C) = r^2
$$

Note: substitute and r^2 into the right hand side. substitute equation for a
point along a ray on the left.

---

$$
t^2 \* b \cdot b + 2 t \* b \cdot (A - C) +
$$

$$
(A - C) \cdot (A - C) - r^2 = 0
$$

Note: If you do the multiplication out, and move r^2 to the left hand side we
get the following. If it wasn't clear before, the asterisk represents normal
multiplication, and the dot represents the dot product. If you look closely we
have a polynomial equation in terms of t, which means we can get solutions to
this equation from the quadratic equation.

---

That was a lot of math, huh?

![sphere roots](./sphere_roots.jpg)

Note: If your eyes just glossed over for the last minute or so I don't blame you
at all. Here's why we care about that whole derivation, its so we can determine
if we've intersected with a sphere in the scene. This same analytical approach
can be taken with other shapes to determine if we've intersected with them.

---

## To the code

- Open up `src/chapter_05/main.c`

```c
bool hit_sphere(Point3 const center, f64 const radius,
                Ray const r) {
  Vec3 const oc = Vec3_sub(r.origin, center);
  f64 const a = Vec3_dot(r.direction, r.direction);
  f64 const b = 2.0 * Vec3_dot(oc, r.direction);
  f64 const c = Vec3_dot(oc, oc) - radius * radius;
  f64 const discriminant = b * b - 4 * a * c;
  return (discriminant > 0);
}
```

Note: Here we calculate a, b, and c from the quadratic formula, then calculate
just the discriminant to see how many roots there are (0, 1, or 2). We then
change ray_color to check if we hit a sphere with a center at 0, 0, -1, and
radius 0.5. If we do, that pixel should be red.

---

<img class="r-stretch" src="chapter_05.png">

---

## A Quick Optimization

$$ \frac{-b \pm \sqrt{b^2 - 4ac}}{2a} $$

$$
= \frac{-2h \pm \sqrt{(2h)^2 -
4ac}}{2a}
$$

$$ = \frac{-2h \pm 2\sqrt{h^2 - ac}}{2a} $$

$$
= \frac{-h \pm
\sqrt{h^2 - ac}}{a}
$$

Note: There are two quick things we can do to make our calculation for
intersecting a sphere faster. The first is to remember that the dot product of a
vector with itself is equal to the magnitude squared of the vector. The second
is that the equation for b has a factor of two in it, which simplify things like
this.

---

```c
bool hit_sphere(Point3 const center, f64 const radius,
                Ray const r) {
  Vec3 const oc = Vec3_sub(r.origin, s.center);
  f64 const a = Vec3_magsq(r.direction);
  f64 const half_b = Vec3_dot(oc, r.direction);
  f64 const c = Vec3_magsq(oc) - s.radius * s.radius;

  f64 const discriminant = half_b * half_b - a * c;
  return (discriminant > 0);
}
```

---

## Surface Normals and Multiple Objects

<img class="r-stretch" src="./surface_normal.jpg">

Note: Now that we have a sphere we can finally start shading things, and to do
that we're going to need a surface normal. This is a vector that is
perpendicular to the surface at the point of intersection. It can face either
inwards or outwards, but we're going to go with outwards for simplicity.

We don't have anything special in the scene yet, so a neat trick we can to do
visualize the surface normals is to assume they're unit vectors with components
between -1 and 1, map them to the interval of 0 and 1, and then map x/y/z to
r/g/b.

---

<img class="r-stretch" src="./colored_by_normals.png">

Note: If you run chapter 6's code you'll see a bit more going on in the scene.
We haven't gotten there yet, but we will very shortly.

---

Open up `include/scatter.h` and `Sphere.c`

Note: To make our code from before for intersecting with spheres easier to work
with, we're going to create a sphere struct to pass around a center and radius
together. At the top of scatter you should see a number of structs defined, but
the ones we care about for now are the sphere and world structs.

---

```c
typedef struct Sphere {
  Point3 center;
  f64 radius;
  Material material;
} Sphere;
```

Note: The sphere struct has a center and a radius, don't worry about the
material type just yet. If you hop into sphere.c you'll see that the code for
intersecting with a sphere from before has moved, and is now placed in the
sphere_hit function. The main change that has been made is instead of just
detecting whether we did or didn't hit, we return a structure containing info on
the point the intersection occurred, and the surface normal facing outwards at
that point.

---

```c
typedef struct World {
  usize num_spheres;
  Sphere* spheres;
} World;
```

Note: The world struct is what we'll use to represent the state of the world. It
contains an integer designating how many spheres it holds, and a pointer to an
unknown number of spheres (hence why we have the previous number to tell us how
many).

If you look in the world_hit function, you'll see that we iterate through the
list of spheres inside the world, and for each sphere determine if we intersect
with the sphere, keeping track of the closest one to the camera so that objects
which are behind each other are shown properly.

---

<img class="r-stretch" src="./chapter_06.png">

---

## Antialiasing

Note: You might have noticed that the edges of the sphere look jagged. When a
real camera takes a picture this isn't a thing because the edge pixels are a
blend of some of the foreground and background. We can get the same effect by
averaging a bunch of samples inside each pixel.

---

<img class="r-stretch" src="./pixel_samples.jpg">

Note: what we'll do is take many samples inside the picture and add each of the
colors we get together, then divide by the number of samples at the end to get
the average.

This adding and averaging is done in the Color_write_two function. The random
sampling is done in the new chapter 7 main file.

---

```c
for (usize i = 0; i < image_width; i += 1) {
  Color color = Color_new(0, 0, 0);
  for (usize s = 0; s < samples_per_pixel; s += 1) {
    f64 const u = (i + random_f64()) / (image_width - 1);
    f64 const v = (j + random_f64()) / (image_height - 1);
    Ray const r = Camera_get_ray(cam, u, v);
    color = Vec3_add(color, ray_color(r, world));
  }
  Color_write_two(color, samples_per_pixel);
}
```

Note: go ahead and run chapter 7, and you should see the edges of the sphere
nicely blended in. The samples_per_pixel value is defined at the top of main,
try changing this value to see how it affects the antialiasing.

---

<img class="r-stretch" src="./chapter_07.png">

---

## Diffuse Materials

Note: Now we can start to make some realistic looking materials, starting with
diffuse (matte) materials.

---

<img class="r-stretch" src="./diffuse_bouncing.jpg">

Note: Diffuse objects that don’t emit light merely take on the color of their
surroundings, but they modulate that with their own intrinsic color. Light that
reflects off a diffuse surface has its direction randomized. So, if we send
three rays into a crack between two diffuse surfaces they will each have
different random behavior.

---

<img class="r-stretch" src="./unit_in_sphere.png">

Note: The method we're going to use for this random reflection is called
lambertian reflection. The way it works is whenever a ray intersects with a
sphere, we're goint o choose a point on the unit sphere in the direction of the
surface normal. From there we're going to recursively continue sending out new
rays until we either miss and don't hit anything, or we reach some arbitrarily
chosen depth. This depth is defined at the top of the main function, play around
with it and see what happens.

---

Open up `src/chapter_08/main.c`

Note: The main place to look is the ray_color function at the top. Because this
is a recursive function we first have to check whether we've reached the base
condition. If we have, then return a color of 0. Next we iterate through
everything in the world, getting back a HitRecord detailing what happened.

Next check whether something was hit. If something was, pick a new target on the
unit sphere, and create a new ray from that. Finally, call the same function
again to try and get a new color. This happens recursively until we miss, or
reach the depth limit.

---

<img class="r-stretch" src="./chapter_08.png">

---

## Sources

<https://raytracing.github.io/books/RayTracingInOneWeekend.html>
<https://github.com/ssloy/tinyraytracer/wiki>

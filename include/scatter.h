#pragma once

#include <stdbool.h>

#include "Color.h"
#include "Ray.h"
#include "Vec3.h"
#include "types.h"

// ----- TYPE DEFINITIONS -----

typedef enum MaterialType {
  Lambertian,
  Metal,
} MaterialType;

typedef struct Material {
  MaterialType type;
  Color albedo;
  f64 fuzz;
} Material;

typedef struct Sphere {
  Point3 center;
  f64 radius;
  Material material;
} Sphere;

typedef struct HitRecord {
  bool did_hit;
  Point3 p;
  Vec3 normal;
  f64 t;
  bool front_face;
  Material material;
} HitRecord;

typedef struct ScatterRecord {
  bool did_scatter;
  Color attenuation;
  Ray scattered;
} ScatterRecord;

typedef struct World {
  usize num_spheres;
  Sphere* spheres;
} World;

// ----- MATERIAL FUNCTIONS -----

Material Material_new(MaterialType const type, Color const albedo);
Material Material_new_lambertian(Color const albedo);
Material Material_new_metal(Color const albedo, f64 const fuzz);

ScatterRecord Material_scatter(Material const mat, Ray const ray,
                               HitRecord const rec);

// ----- SPHERE FUNCTIONS -----

Sphere Sphere_init(void);
Sphere Sphere_new(Point3 const center, f64 const radius);
Sphere Sphere_with_material(Point3 const center, f64 const radius,
                            Material material);

HitRecord Sphere_hit(Sphere const s, Ray const r, f64 const t_min,
                     f64 const t_max);

// ----- WORLD FUNCTIONS -----

HitRecord World_hit(World const w, Ray const r, f64 const t_min,
                    f64 const t_max);

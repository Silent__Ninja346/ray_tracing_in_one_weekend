#pragma once

#include <stdbool.h>

#include "types.h"

typedef struct Vec3 {
  union {
    f64 xyz[3];
    struct {
      f64 x;
      f64 y;
      f64 z;
    };
  };
} Vec3;

Vec3 Vec3_init(void);
Vec3 Vec3_new(f64 const x, f64 const y, f64 const z);

Vec3 Vec3_negate(Vec3 const u);

Vec3 Vec3_add(Vec3 const u, Vec3 const v);
Vec3 Vec3_sub(Vec3 const u, Vec3 const v);
Vec3 Vec3_mul(f64 const u, Vec3 const v);
Vec3 Vec3_elementwise_mul(Vec3 const u, Vec3 const v);

Vec3 Vec3_cross(Vec3 const u, Vec3 const v);
Vec3 Vec3_unit_vec(Vec3 const direction);
Vec3 Vec3_reflect(Vec3 const v, Vec3 const n);

f64 Vec3_dot(Vec3 const u, Vec3 const v);
f64 Vec3_mag(Vec3 const u);
f64 Vec3_magsq(Vec3 const u);

bool Vec3_near_zero(Vec3 const u);

typedef Vec3 Point3;
Point3 Point3_new(f64 const r, f64 const g, f64 const b);

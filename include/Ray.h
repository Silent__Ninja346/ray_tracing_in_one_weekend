#pragma once

#include "Vec3.h"
#include "types.h"

typedef struct Ray {
  Vec3 origin;
  Vec3 direction;
} Ray;

Ray Ray_new(Vec3 const u, Vec3 const v);

Vec3 Ray_at(Ray const r, f64 const t);

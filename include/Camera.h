#pragma once

#include "Ray.h"
#include "Vec3.h"
#include "types.h"

typedef struct Camera {
  Point3 origin;
  Point3 lower_left_corner;
  Vec3 horizontal;
  Vec3 vertical;
} Camera;

Camera Camera_init(void);

Ray Camera_get_ray(Camera const c, f64 const u, f64 const v);

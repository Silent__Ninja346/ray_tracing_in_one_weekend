#pragma once

#include "Vec3.h"
#include "types.h"

typedef Vec3 Color;

Color Color_new(f64 const, f64 const, f64 const);

void Color_write_one(Color const color);
void Color_write_two(Color const color, usize const samples_per_pixel);
void Color_write_three(Color const color, usize const samples_per_pixel);

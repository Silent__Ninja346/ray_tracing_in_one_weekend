#pragma once

#include "Vec3.h"
#include "types.h"

// Constants
extern f64 const PI;

f64 degrees_to_radians(f64 const degrees);

f64 random_f64(void);
f64 random_f64_in_range(f64 const min, f64 const max);

f64 clamp(f64 const x, f64 const min, f64 const max);

Vec3 random_vec(void);
Vec3 random_vec_in_range(f64 const min, f64 const max);
Vec3 random_vec_in_unit_sphere(void);
Vec3 random_unit_vec(void);

#include "Color.h"

#include <inttypes.h>
#include <stdio.h>
#include <tgmath.h>

#include "util.h"

Color Color_new(f64 const r, f64 const g, f64 const b) {
  return Vec3_new(r, g, b);
}

void Color_write_one(Color const color) {
  u32 const r = 255.999 * color.x;
  u32 const g = 255.999 * color.y;
  u32 const b = 255.999 * color.z;
  printf("%" PRIu32 " %" PRIu32 " %" PRIu32 "\n", r, g, b);
}
void Color_write_two(Color const color, usize const samples_per_pixel) {
  f64 r = color.x;
  f64 g = color.y;
  f64 b = color.z;

  f64 const scale = 1.0 / samples_per_pixel;
  r *= scale;
  g *= scale;
  b *= scale;

  u32 const ir = 256 * clamp(r, 0.0, 0.999);
  u32 const ig = 256 * clamp(g, 0.0, 0.999);
  u32 const ib = 256 * clamp(b, 0.0, 0.999);

  printf("%" PRIu32 " %" PRIu32 " %" PRIu32 "\n", ir, ig, ib);
}
void Color_write_three(Color const color, usize const samples_per_pixel) {
  f64 r = color.x;
  f64 g = color.y;
  f64 b = color.z;

  f64 const scale = 1.0 / samples_per_pixel;
  r = sqrt(scale * r);
  g = sqrt(scale * g);
  b = sqrt(scale * b);

  u32 const ir = 256 * clamp(r, 0.0, 0.999);
  u32 const ig = 256 * clamp(g, 0.0, 0.999);
  u32 const ib = 256 * clamp(b, 0.0, 0.999);

  printf("%" PRIu32 " %" PRIu32 " %" PRIu32 "\n", ir, ig, ib);
}

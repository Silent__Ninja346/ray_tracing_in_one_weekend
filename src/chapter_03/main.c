#include <inttypes.h>
#include <stdio.h>

#include "Color.h"
#include "types.h"

i32 main(void) {
  u32 const image_width = 256;
  u32 const image_height = 256;

  printf("P3\n%" PRIu32 " %" PRIu32 "\n255\n", image_width, image_height);

  u32 const h_max = image_height - 1;
  for (usize j = h_max; j <= h_max; j -= 1) {
    fprintf(stderr, "\rScanlines remaining: %zu ", j);
    for (usize i = 0; i < image_width; i += 1) {
      Color const pixel_color =
          Color_new((f64)i / (image_width - 1), (f64)j / (h_max), 0.25);
      Color_write_one(pixel_color);
    }
  }
  fputs("\nDone.\n", stderr);
}

#include <inttypes.h>
#include <stdio.h>

#include "types.h"

i32 main(void) {
  u32 const image_width = 256;
  u32 const image_height = 256;

  // Output is sent to stdout, so redirect it to a file to save it
  printf("P3\n%" PRIu32 " %" PRIu32 "\n255\n", image_width, image_height);

  u32 const h_max = image_height - 1;
  // Pixels are written out from top to bottom
  // Fun fact, this loop probably looks like it'll never end!
  // Lucky for us unsigned numbers will over and underflow, so after the going
  // below zero j becomes some number bigger than h_max (255 in this case), and
  // the loop ends. This isn't guaranteed to happen with signed numbers!
  for (usize j = h_max; j <= h_max; j -= 1) {
    // Progress indicator
    fprintf(stderr, "\rScanlines remaining: %zu ", j);
    // Pixels are written from left to write
    for (usize i = 0; i < image_width; i += 1) {
      // By convention, color values will internally range from 0.0 to 1.0
      // This will be relaxed later, but we'll tone map to the zero to one
      // range.
      f64 const r = (f64)i / (image_width - 1);
      f64 const g = (f64)j / (h_max);
      f64 const b = 0.25;

      u32 const ir = (int)(255.999 * r);
      u32 const ig = (int)(255.999 * g);
      u32 const ib = (int)(255.999 * b);

      printf("%" PRIu32 " %" PRIu32 " %" PRIu32 "\n", ir, ig, ib);
    }
  }
  fputs("\nDone.\n", stderr);
}

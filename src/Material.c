#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "Color.h"
#include "Ray.h"
#include "Vec3.h"
#include "scatter.h"
#include "types.h"
#include "util.h"

Material Material_new(MaterialType const type, Color const albedo) {
  Material const m = {
      .type = type,
      .albedo = albedo,
  };
  return m;
}
Material Material_new_lambertian(Color const albedo) {
  Material const m = {
      .type = Lambertian,
      .albedo = albedo,
  };
  return m;
}
Material Material_new_metal(Color const albedo, f64 const fuzz) {
  Material const m = {
      .type = Metal,
      .albedo = albedo,
      .fuzz = fuzz,
  };
  return m;
}

static ScatterRecord lambertian_scatter(Material const mat,
                                        HitRecord const rec) {
  Vec3 scatter_direction = Vec3_add(rec.normal, random_unit_vec());
  if (Vec3_near_zero(scatter_direction)) {
    scatter_direction = rec.normal;
  }
  ScatterRecord const s = {
      .did_scatter = true,
      .scattered = Ray_new(rec.p, scatter_direction),
      .attenuation = mat.albedo,
  };
  return s;
}
static ScatterRecord metal_scatter(Material const mat, Ray const ray,
                                   HitRecord const rec) {
  Vec3 const unit = Vec3_unit_vec(ray.direction);
  Vec3 const reflected = Vec3_reflect(unit, rec.normal);
  Vec3 const rand = random_vec_in_unit_sphere();
  Vec3 const temp = Vec3_mul(mat.fuzz, rand);
  Ray const scattered = Ray_new(rec.p, Vec3_add(reflected, temp));
  ScatterRecord const s = {
      .did_scatter = Vec3_dot(scattered.direction, rec.normal) > 0,
      .scattered = scattered,
      .attenuation = mat.albedo,
  };
  return s;
}

ScatterRecord Material_scatter(Material const mat, Ray const ray,
                               HitRecord const rec) {
  switch (mat.type) {
    case Lambertian:
      return lambertian_scatter(mat, rec);
    case Metal:
      return metal_scatter(mat, ray, rec);
    default:
      puts("Received non existent material type, exiting");
      exit(EXIT_FAILURE);
  }
}

#include <inttypes.h>
#include <math.h>
#include <stdio.h>

#include "Camera.h"
#include "Color.h"
#include "Ray.h"
#include "Vec3.h"
#include "scatter.h"
#include "types.h"
#include "util.h"

Color ray_color(Ray const r, World const w, u32 const depth) {
  if (depth <= 0) {
    return Color_new(0, 0, 0);
  }
  HitRecord const rec = World_hit(w, r, 0.001, INFINITY);
  if (rec.did_hit) {
    Point3 const target =
        Vec3_add(Vec3_add(rec.p, rec.normal), random_unit_vec());
    Vec3 const target_minus_p = Vec3_sub(target, rec.p);
    Ray const new_ray = Ray_new(rec.p, target_minus_p);
    Color const new_color = ray_color(new_ray, w, depth - 1);
    return Vec3_mul(0.5, new_color);
  }

  Vec3 const unit_direction = Vec3_unit_vec(r.direction);
  f64 const t = 0.5 * (unit_direction.y + 1);
  return Vec3_add(Vec3_mul(1.0 - t, Color_new(1.0, 1.0, 1.0)),
                  Vec3_mul(t, Color_new(0.5, 0.7, 1.0)));
}

i32 main(void) {
  // Image
  f64 const aspect_ratio = 16.0 / 9.0;
  u32 const image_width = 400;
  u32 const image_height = (int)(image_width / aspect_ratio);
  u32 const samples_per_pixel = 100;
  u32 const max_depth = 50;

  // World
  World const world = {.num_spheres = 2,
                       .spheres = (Sphere[]){
                           Sphere_new(Point3_new(0, 0, -1), 0.5),
                           Sphere_new(Point3_new(0, -100.5, -1), 100),
                       }};

  // Camera
  Camera const cam = Camera_init();

  // Render
  printf("P3\n%" PRIu32 " %" PRIu32 "\n255\n", image_width, image_height);

  u32 const h_max = image_height - 1;
  for (usize j = h_max; j <= h_max; j -= 1) {
    fprintf(stderr, "\rScanlines remaining: %zu ", j);
    for (usize i = 0; i < image_width; i += 1) {
      Color color = Color_new(0, 0, 0);
      for (usize s = 0; s < samples_per_pixel; s += 1) {
        f64 const u = (i + random_f64()) / (image_width - 1);
        f64 const v = (j + random_f64()) / (image_height - 1);
        Ray const r = Camera_get_ray(cam, u, v);
        color = Vec3_add(color, ray_color(r, world, max_depth));
      }
      Color_write_three(color, samples_per_pixel);
    }
  }
  fputs("\nDone.\n", stderr);
}

#include "util.h"

#include <stdbool.h>
#include <stdlib.h>

#include "Vec3.h"

f64 const PI = 3.1415926535897932385;

f64 degrees_to_radians(f64 const degrees) { return degrees * PI / 180.0; }

f64 random_f64(void) { return rand() / (RAND_MAX + 1.0); }
f64 random_f64_in_range(f64 const min, f64 const max) {
  return min + (max - min) * random_f64();
}

f64 clamp(f64 const x, f64 const min, f64 const max) {
  if (x < min) {
    return min;
  }
  if (x > max) {
    return max;
  }
  return x;
}

Vec3 random_vec(void) {
  return Vec3_new(random_f64(), random_f64(), random_f64());
}
Vec3 random_vec_in_range(f64 const min, f64 const max) {
  return Vec3_new(random_f64_in_range(min, max), random_f64_in_range(min, max),
                  random_f64_in_range(min, max));
}
Vec3 random_vec_in_unit_sphere(void) {
  while (true) {
    Vec3 p = random_vec_in_range(-1, 1);
    if (Vec3_magsq(p) >= 1) {
      continue;
    }
    return p;
  }
}
Vec3 random_unit_vec(void) {
  return Vec3_unit_vec(random_vec_in_unit_sphere());
}

#include "Vec3.h"

#include <tgmath.h>

Vec3 Vec3_init(void) {
  Vec3 const w = {0};
  return w;
}
Vec3 Vec3_new(f64 const x, f64 const y, f64 const z) {
  Vec3 const w = {
      .x = x,
      .y = y,
      .z = z,
  };
  return w;
}

Vec3 Vec3_negate(Vec3 const u) {
  Vec3 const w = {
      .x = -u.x,
      .y = -u.y,
      .z = -u.z,
  };
  return w;
}

Vec3 Vec3_add(Vec3 const u, Vec3 const v) {
  Vec3 const w = {
      .x = u.x + v.x,
      .y = u.y + v.y,
      .z = u.z + v.z,
  };
  return w;
}
Vec3 Vec3_sub(Vec3 const u, Vec3 const v) {
  Vec3 const w = {
      .x = u.x - v.x,
      .y = u.y - v.y,
      .z = u.z - v.z,
  };
  return w;
}
Vec3 Vec3_mul(f64 const t, Vec3 const u) {
  Vec3 const w = {
      .x = t * u.x,
      .y = t * u.y,
      .z = t * u.z,
  };
  return w;
}
Vec3 Vec3_elementwise_mul(Vec3 const u, Vec3 const v) {
  Vec3 const w = {
      .x = u.x * v.x,
      .y = u.y * v.y,
      .z = u.z * v.z,
  };
  return w;
}

Vec3 Vec3_cross(Vec3 const u, Vec3 const v) {
  Vec3 const w = {
      .x = u.y * v.z - u.z * v.y,
      .y = u.z * v.x - u.x * v.z,
      .z = u.x * v.y - u.y * v.x,
  };
  return w;
}
Vec3 Vec3_unit_vec(Vec3 const direction) {
  f64 const t = sqrt(direction.x * direction.x + direction.y * direction.y +
                     direction.z * direction.z);
  Vec3 const v = {.x = direction.x / t, .y = direction.y / t, direction.z / t};
  return v;
}
Vec3 Vec3_reflect(Vec3 const v, Vec3 const n) {
  f64 const dot = Vec3_dot(v, n);
  Vec3 const two_b = Vec3_mul(2 * dot, n);
  return Vec3_sub(v, two_b);
}

f64 Vec3_dot(Vec3 const u, Vec3 const v) {
  return u.x * v.x + u.y * v.y + u.z * v.z;
}
f64 Vec3_mag(Vec3 const u) {
  f64 const t = sqrt(Vec3_dot(u, u));
  return t;
}
f64 Vec3_magsq(Vec3 const u) {
  f64 const t = Vec3_dot(u, u);
  return t;
}

bool Vec3_near_zero(Vec3 const u) {
  f64 const s = 1e-8;
  return (fabs(u.x) < s) && (fabs(u.y) < s) && (fabs(u.z) < s);
}

Point3 Point3_new(f64 const r, f64 const g, f64 const b) {
  return Vec3_new(r, g, b);
}

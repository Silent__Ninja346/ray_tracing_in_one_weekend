#include "Camera.h"

#include "Ray.h"
#include "Vec3.h"

Camera Camera_init(void) {
  f64 aspect_ratio = 16.0 / 9.0;
  f64 viewport_height = 2.0;
  f64 viewport_width = aspect_ratio * viewport_height;
  f64 focal_length = 1.0;

  Vec3 const origin = Point3_new(0, 0, 0);
  Vec3 const horizontal = Vec3_new(viewport_width, 0.0, 0.0);
  Vec3 const vertical = Vec3_new(0.0, viewport_height, 0.0);
  Camera const c = {
      .origin = origin,
      .horizontal = horizontal,
      .vertical = vertical,
      .lower_left_corner = Vec3_new(
          origin.x - horizontal.x / 2.0 - vertical.x / 2.0,
          origin.y - horizontal.y / 2.0 - vertical.y / 2.0,
          origin.z - horizontal.z / 2.0 - vertical.z / 2.0 - focal_length),
  };
  return c;
}

Ray Camera_get_ray(Camera const c, f64 const u, f64 const v) {
  Vec3 const uhoriz = Vec3_mul(u, c.horizontal);
  Vec3 const vverti = Vec3_mul(v, c.vertical);
  Vec3 dir = Vec3_init();

  for (usize i = 0; i < 3; i += 1) {
    dir.xyz[i] = c.lower_left_corner.xyz[i] + uhoriz.xyz[i] + vverti.xyz[i] -
                 c.origin.xyz[i];
  }
  return Ray_new(c.origin, dir);
}

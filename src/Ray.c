#include "Ray.h"

Ray Ray_new(Vec3 const u, Vec3 const v) {
  Ray const r = {
      .origin = u,
      .direction = v,
  };
  return r;
}

Vec3 Ray_at(Ray const r, f64 const t) {
  Vec3 const t_dir = Vec3_mul(t, r.direction);
  return Vec3_add(r.origin, t_dir);
}

#include <inttypes.h>
#include <math.h>
#include <stdio.h>

#include "Color.h"
#include "Ray.h"
#include "Vec3.h"
#include "scatter.h"
#include "types.h"

Color ray_color(Ray const r, World const w) {
  HitRecord const rec = World_hit(w, r, 0, INFINITY);
  if (rec.did_hit) {
    return Vec3_mul(0.5, Vec3_add(rec.normal, Color_new(1, 1, 1)));
  }

  Vec3 const unit_direction = Vec3_unit_vec(r.direction);
  f64 const t = 0.5 * (unit_direction.y + 1);
  return Vec3_add(Vec3_mul(1.0 - t, Color_new(1.0, 1.0, 1.0)),
                  Vec3_mul(t, Color_new(0.5, 0.7, 1.0)));
}

i32 main(void) {
  // Image
  f64 const aspect_ratio = 16.0 / 9.0;
  u32 const image_width = 400;
  u32 const image_height = (int)(image_width / aspect_ratio);

  // World
  World const world = {.num_spheres = 2,
                       .spheres = (Sphere[]){
                           Sphere_new(Point3_new(0, 0, -1), 0.5),
                           Sphere_new(Point3_new(0, -100.5, -1), 100),
                       }};

  // Camera
  f64 const viewport_height = 2.0;
  f64 const viewport_width = aspect_ratio * viewport_height;
  f64 const focal_length = 1.0;

  Point3 const origin = Vec3_new(0, 0, 0);
  Vec3 const horizontal = Vec3_new(viewport_width, 0, 0);
  Vec3 const vertical = Vec3_new(0, viewport_height, 0);
  Vec3 lower_left_corner = Vec3_init();
  lower_left_corner.x = origin.x - horizontal.x / 2.0 - vertical.x / 2.0;
  lower_left_corner.y = origin.y - horizontal.y / 2.0 - vertical.y / 2.0;
  lower_left_corner.z =
      origin.z - horizontal.z / 2.0 - vertical.z / 2.0 - focal_length;

  printf("P3\n%" PRIu32 " %" PRIu32 "\n255\n", image_width, image_height);

  u32 const h_max = image_height - 1;
  for (usize j = h_max; j <= h_max; j -= 1) {
    fprintf(stderr, "\rScanlines remaining: %zu ", j);
    for (usize i = 0; i < image_width; i += 1) {
      f64 const u = (f64)i / (image_width - 1);
      f64 const v = (f64)j / (image_height - 1);

      Vec3 const direction = Vec3_new(
          lower_left_corner.x + u * horizontal.x + v * vertical.x - origin.x,
          lower_left_corner.y + u * horizontal.y + v * vertical.y - origin.y,
          lower_left_corner.z + u * horizontal.z + v * vertical.z - origin.z);

      Ray const r = Ray_new(origin, direction);
      Color const pixel_color = ray_color(r, world);
      Color_write_one(pixel_color);
    }
  }
  fputs("\nDone.\n", stderr);
}

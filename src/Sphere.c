#include <stdbool.h>
#include <tgmath.h>

#include "Ray.h"
#include "Vec3.h"
#include "scatter.h"
#include "types.h"

Sphere Sphere_init(void) {
  Sphere const s = {0};
  return s;
}
Sphere Sphere_new(Point3 const center, f64 const radius) {
  Sphere const s = {
      .center = center,
      .radius = radius,
  };
  return s;
}
Sphere Sphere_with_material(Point3 const center, f64 const radius,
                            Material material) {
  Sphere const s = {
      .center = center,
      .radius = radius,
      .material = material,
  };
  return s;
}

HitRecord Sphere_hit(Sphere const s, Ray const r, f64 const t_min,
                     f64 const t_max) {
  Vec3 const oc = Vec3_sub(r.origin, s.center);
  f64 const a = Vec3_magsq(r.direction);
  f64 const half_b = Vec3_dot(oc, r.direction);
  f64 const c = Vec3_magsq(oc) - s.radius * s.radius;

  f64 const discriminant = half_b * half_b - a * c;
  if (discriminant < 0) {
    return (HitRecord){.did_hit = false};
  }
  f64 const sqrtd = sqrt(discriminant);

  f64 root = (-half_b - sqrtd) / a;
  if (root < t_min || t_max < root) {
    root = (-half_b + sqrtd) / a;
    if (root < t_min || t_max < root) {
      return (HitRecord){.did_hit = false};
    }
  }

  HitRecord rec = {0};
  rec.did_hit = true;
  rec.t = root;
  rec.p = Ray_at(r, root);
  Vec3 temp = Vec3_sub(rec.p, s.center);
  rec.normal = Vec3_mul(1 / s.radius, temp);
  rec.front_face = Vec3_dot(r.direction, rec.normal);
  rec.material = s.material;

  return rec;
}

HitRecord World_hit(World const w, Ray const r, f64 const t_min,
                    f64 const t_max) {
  HitRecord return_rec = {
      .did_hit = false,
      .t = t_max,
  };

  for (usize i = 0; i < w.num_spheres; i += 1) {
    Sphere const s = w.spheres[i];
    HitRecord const tmp_rec = Sphere_hit(s, r, t_min, return_rec.t);
    if (tmp_rec.did_hit) {
      return_rec = tmp_rec;
    }
  }

  return return_rec;
}

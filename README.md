<!-- SPDX-License-Identifier: MIT OR Apache-2.0 -->

# Ray Tracing In One Weekend in Glorious C

[![License](https://img.shields.io/badge/license-MIT%2FApache--2.0-informational?style=flat-square)](COPYRIGHT.md)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Yet another rendition of the classic
[Ray Tracing in One Weekend](https://github.com/RayTracing/raytracing.github.io)
by Peter Shirley. The original is implemented in C++, but all these newfangled
things like "objects" and "operator overloading" are overrated. In all
seriousness, I've used C++ before, but could never really get past exceptions
being terrible compared to the Result type in Rust, which oddly enough drove me
to do this thing in C.

This repository is also meant to be a companion for a workshop for the Stevens
IEEE club. I've done what I can to make the code as legible as possible, and
intentionally avoided using pointers (even if it might be less efficient) for
the sake of making it easy for freshman who've only seen basic constructs like
loops before.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Install

You're going to need a C compiler. Get one however you like, but I've only
tested building this on Linux with gcc and clang. (Windows testing inbound).

Next, clone the repository:

```shell
git clone "funnyurlhere"
```

Then, build the project with Meson:

```shell
meson setup build
cd build
ninja
```

## Usage

After building the project there will be executables in build folder for each of
the individual chapters, which you can run as the following:

```shell
./chapter_XX > image_name.ppm
```

Replace XX with the chapter number, and filename after the redirection (">") can
be whatever you like.

The image output is in the
[Portable Pixmap](https://en.wikipedia.org/wiki/Portable_pixmap) format.
Gwenview on Linux is able to handle it, but I'm sure there are viewers online
somewhere, or you can always convert the image with ImageMagick if you have that
handy.

## Contributing

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as below, without any additional terms or conditions.

## License

&copy; 2022 David Krauthamer.

This project is licensed under either of

- [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
  ([`LICENSE-APACHE`](LICENSE-APACHE))
- [MIT license](https://opensource.org/licenses/MIT)
  ([`LICENSE-MIT`](LICENSE-MIT))

at your option.

The [SPDX](https://spdx.dev) license identifier for this project is
`MIT OR Apache-2.0`.
